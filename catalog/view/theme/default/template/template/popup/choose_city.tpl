<div id="form-<?php echo $pre_id; ?>" class="fixed">
	<div class="close-form"></div>
	<div class="cell-city">
		<div class="form-city">
			<div class="close"><svg viewBox="0 0 16 16" width="10" height="10"><path d="M9.3 8l6.7 6.7-1.3 1.3L8 9.3 1.3 16 0 14.7 6.7 8 0 1.3 1.3 0 8 6.7 14.7 0 16 1.3 9.3 8z"></path></svg></div>
			<div class="title">Выбор города</div>
			<div class="scroll">

				<div class="search-city">
					<form>
      					<span>
      						<input class="place_for_search" type="text" id="text-to-find" value="" placeholder="поиск" autofocus>
      					</span>
      					<span>	
      					<i class="pr loop"></i>
      					</span>
					</form>
					<script>
					document.addEventListener("DOMContentLoaded", e => {
					document.querySelector('.search-city form').addEventListener('submit', e => {
						e.preventDefault();
						//тут действия по сабиту формы
					});
					});
					document.addEventListener('input', e => {
					if (e.target.attributes.id.nodeValue == 'text-to-find') {
					var $cities = document.querySelectorAll('.cities li');
					$cities.forEach(city => {
					city.style.display = '';
					if (city.querySelector('span').innerHTML.toLowerCase().indexOf(e.target.value.toLowerCase()) == -1) {
					city.style.display = 'none';
					}
					});
					}
					})
					</script>
				</div>

				<div class="big-cities-bubble-list">
					<span data-role="big-cities" class="city-bubble">
						<a href="https://engproff.ru" rel="nofollow noopener">Москва</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://spb.engproff.ru" rel="nofollow noopener">Санкт-Петербург</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://novosibirsk.engproff.ru" rel="nofollow noopener">Новосибирск</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://ekb.engproff.ru" rel="nofollow noopener">Екатеринбург</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://nizhniy-novgorod.engproff.ru/" rel="nofollow noopener">Нижний Новогород</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://kazan.engproff.ru/" rel="nofollow noopener">Казань</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://samara.engproff.ru/" rel="nofollow noopener">Самара</a>
					</span>
					<span data-role="big-cities" class="city-bubble">
						<a href="https://vladivostok.engproff.ru/" rel="nofollow noopener">Владивосток</a>
					</span>			
			</div>
			<div class="lists-column">
				<ul id="info" class="cities">


<?php
				foreach($city_domen as $i => $domen)
				{
?>				
					<li class="modal-row">
						<a href="<?php echo $domen; ?>" rel="nofollow noopener"><span><?php echo $city_name[$i]; ?></span></a>
					</li>				
<?php				
				}

?>
					
	

				</ul>	
			</div
		</div>
	</div>
</div>

<script>
	$('body').on('click', '#form-<?php echo $pre_id; ?> .close-form, #form-<?php echo $pre_id; ?> .close', function(){
	$('body').find('#form-<?php echo $pre_id; ?>').remove();
	$('body').css({overflow: ''});
});
</script>