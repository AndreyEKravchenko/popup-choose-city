<?php
class ControllerPopupPopup extends Controller {
	public function index() {
		
		$type = $this->request->get['type'];
		$product = (isset($this->request->get['product'])) ? $this->request->get['product'] : false;
		$link	= (isset($this->request->get['link'])) ? $this->request->get['link'] : false;
		$product_id	= (isset($this->request->get['product_id'])) ? $this->request->get['product_id'] : false;
		$thumb = false;
		$sku = false;
		$minimum = 1;
		$products = false;
		$iframe_src = false;
		$iframe_title = false;
		$shipping = false;
		
		if(isset($this->request->get['iframe_src']))
			$iframe_src = $this->request->get['iframe_src'];
		
		if(isset($this->request->get['iframe_title']))
			$iframe_title = $this->request->get['iframe_title'];
		
		if(isset($this->request->get['shipping']))
			$shipping = $this->request->get['shipping'];
		
		if(isset($this->request->get['order'])){
			
			$cart_products = $this->cart->getProducts();
			
			foreach ($cart_products as $product) {
				
				$product_info = $this->model_catalog_product->getProduct($product['product_id']);
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']);
				} else {
					$total = false;
				}
				
				$products[] = array(
					'name'      => $product['name'],
					'sku'     => $product_info['sku'],
					'quantity'  => $product['quantity'],
					'price'     => $price,
					'total'     => $total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
				
			}
			
		}
		
		if($product_id){
			
			$this->load->model('tool/image');
			
			$query = $this->db->query("SELECT image, sku, minimum FROM ". DB_PREFIX ."product WHERE product_id = '". (int)$product_id ."' LIMIT 1");
			if(isset($query->row['image'])){
				$thumb = $this->model_tool_image->resize($query->row['image'], 60, 60);
				$sku = $query->row['sku'];
				$minimum = $query->row['minimum'];
			}else{
				$thumb = $this->model_tool_image->resize('placeholder.png', 60, 60);
			}
			
			$link = $this->url->link('product/product', 'product_id=' . $product_id);
			
		}
		
		$data = array();
		
		$data['product'] = $product;
		$data['products'] = $products;
		$data['product_id'] = $product_id;
		$data['link'] = $link;
		$data['thumb'] = $thumb;
		$data['sku'] = $sku;
		$data['minimum'] = $minimum;
		$data['iframe_src'] = $iframe_src;
		$data['iframe_title'] = $iframe_title;
		$data['shipping'] = $shipping;
		
		$data['home'] = HTTPS_SERVER;
			
		$this->load->language('popup/popup');
			
		$data['text_name'] = $this->language->get('text_name');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_question'] = $this->language->get('text_question');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_message'] = $this->language->get('text_message');
		$data['text_error_found'] = $this->language->get('text_error_found');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_product_link'] = $this->language->get('text_product_link');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_send'] = $this->language->get('text_send');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_sended'] = $this->language->get('text_sended');
		$data['text_sended_info'] = $this->language->get('text_sended_info');
		$data['text_link_lower'] = $this->language->get('text_link_lower');
		$data['text_error'] = $this->language->get('text_error');
		$data['text_error_info'] = sprintf($this->language->get('text_error_info'), $this->config->get('config_telephone'), $this->config->get('config_email'));
		$data['text_success'] = $this->language->get('text_success');
		
		$this->load->model('catalog/information');
		
		$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

		if ($information_info) {
			$data['text_agree'] = sprintf($this->language->get('text_agree'), $data['text_send'], '/privacy/', $information_info['title'], $information_info['title']);
		} else {
			$data['text_agree'] = '';
		}
			
		$data['animate_name'] = $this->language->get('animate_name');
		$data['animate_email'] = $this->language->get('animate_email');
		$data['animate_login'] = $this->language->get('animate_login');
		$data['animate_telephone'] = $this->language->get('animate_telephone');
		$data['animate_question'] = $this->language->get('animate_question');
		$data['animate_password'] = $this->language->get('animate_password');
		$data['animate_message'] = $this->language->get('animate_message');
		$data['animate_error_found'] = $this->language->get('animate_error_found');
		$data['animate_price'] = $this->language->get('animate_price');
		$data['animate_product_link'] = $this->language->get('animate_product_link');

				//KAE
		$query_all_city = $this->db->query("SELECT id, domain FROM `oc_regional_store` WHERE status=1");

		$city_data = array(
			0 => "Москва",
			1 => "Ростов-на-Дону",
			2 => "Екатеренбург"
		);
		
		$domen_data = array(
			0 => "https://engproff.ru",
			1 => "https://rnd.engproff.ru",
			2 => "https://ekb.engproff.ru"
		);
		$i=0;
		foreach ($query_all_city->rows as $row) {

			$domen_data[$i] = $row['domain'];
			$name = $this->db->query("SELECT city FROM `oc_regional_store_settings` WHERE id_regional_store={$row['id']}");
			$city_data[$i] = $name->row['city'];//$row['domain'];
			$i = $i + 1;		
			//print_r($row['id']);
		}


		
		
						//print_r($query_all_city->rows[1][id]);
										// Array
										// (
										//     [id] => 175
										//     [domain] => https://engproff.ru/

		
		

			//foreach($city_domen as $i => $domen)
			// while ($row = mysql_fetch_array($query_all_city, MYSQL_NUM))) {

			// }
			// $rows = mysql_fetch_array($query_all_city, MYSQL_NUM);
			
		 // 	while ($rows = mysql_fetch_array($query_all_city, MYSQL_NUM)) 

	   //  	{    
	 //        	$city_data[$i] = $this->db->query("SELECT city FROM `oc_regional_store_settings` WHERE id_regional_store=$row[0]");        	
		// 		$domen_data[$i]  = $row[1];
		 		
	     //	}
	 //    	$data['citidomen'] = $this->domen_data;
		// 	$data['city_name'] = $this->city_data;
	 //   } else {
	 //     	//$data['city_domen'] = "Moscow"
		//  	//$data['city_name'] = "Moscow";
	 //    }
		
		
	    $data['city_domen'] = $domen_data;
		$data['city_name'] = $city_data;
	    //}
    	//KAE - end







		
		
		if($this->customer->isLogged()){
				
			$data['customer_name'] = $this->customer->getFirstName();
			$data['customer_email'] = $this->customer->getEmail();
			$data['customer_telephone'] = $this->customer->getTelephone();
				
		}else{
				
			$data['customer_name'] = '';
			$data['customer_email'] = '';
			$data['customer_telephone'] = '';
				
		}
		
		$data['pre_id'] =  str_replace('.', '_', microtime(true));

		$this->response->setOutput($this->load->view('popup/'.$type, $data));
		
	}
	
	public function login(){
		
		$json = array();
		
		$this->load->language('account/login');
		$this->load->model('account/customer');
		
		$valid = $this->loginValidate($this->request->post);
		
		if(!isset($valid['error'])){
			// Unset guest
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('login', $activity_data);
			}

			$json['success'] = 1;
					
		}else{
			$json['error'] = $valid['error'];
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	
	protected function loginValidate($data) {
		
		$result = array();
		
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($data['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$result['error'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($data['email']);

		if ($customer_info && !$customer_info['approved']) {
			$result['error'] = $this->language->get('error_approved');
		}

		if (!isset($result['error'])) {
			if (!$this->customer->login($data['email'], $data['password'])) {
				$result['error'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($data['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($data['email']);
			}
		}

		return $result;
	}
	
	public function oneClick() {
		$this->load->language('checkout/one_click');
		
		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (isset($this->request->post['quantity'])) {
			$quantity = (int)$this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
      				$order_data['products'] = array();
					$option_data = array();
                    $order_data['products'][] = array(
							'product_id' => $product_info['product_id'],
							'name'       => $product_info['name'],
							'model'      => $product_info['model'],
							'option'     => $option_data,
							'download'   => '',
							'quantity'   => $quantity,
							'subtract'   => $product_info['subtract'],
							'price'      => $product_info['price'],
							'total'      => $product_info['price'] * $quantity,
							'tax'        => '',
							'reward'     => $product_info['reward']
						);
					
                    $order_data['vouchers'] = array();
                    $order_data['totals'][] = array(
                        'code'          => 'total',
                        'title'         => 'Итого',
                        'value'         => $product_info['price'] * $quantity,
                        'sort_order'    => 9 
                    );
                    
                    $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
                    $order_data['store_id'] = $this->config->get('config_store_id');
                    $order_data['store_name'] = $this->config->get('config_name');
                    
                    if ($order_data['store_id']) {
                        $order_data['store_url'] = $this->config->get('config_url');
                    } else {
                        $order_data['store_url'] = HTTP_SERVER;
                    }
					
                    $order_data['customer_id'] = 0;
                    $order_data['customer_group_id'] = 1;
                    $order_data['firstname'] = $this->request->post['name'];
                    $order_data['lastname'] = 'OneClick';
                    $order_data['email'] = (!isset($this->request->post['email']) || empty($this->request->post['email'])) ? $this->config->get('config_email') : $this->request->post['email'];
                    $order_data['telephone'] = $this->request->post['telephone'];
                    $order_data['fax'] = '';
                    $order_data['custom_field'] = array();
                    
					$order_data['payment_firstname'] = '';
                    $order_data['payment_lastname'] = '';
                    $order_data['payment_company'] = '';
                    $order_data['payment_address_1'] = '';
                    $order_data['payment_address_2'] = '';
                    $order_data['payment_city'] = '';
                    $order_data['payment_postcode'] = '';
                    $order_data['payment_zone'] = '';
                    $order_data['payment_zone_id'] = '';
                    $order_data['payment_country'] = '';
                    $order_data['payment_country_id'] = '';
                    $order_data['payment_address_format'] = '';
                    $order_data['payment_custom_field'] = array();
                    $order_data['payment_method'] = '';
					$order_data['payment_code'] = 'cod';
            
					$order_data['shipping_firstname'] = '';
                    $order_data['shipping_lastname'] = '';
                    $order_data['shipping_company'] = '';
                    $order_data['shipping_address_1'] = '';
                    $order_data['shipping_address_2'] = '';
                    $order_data['shipping_city'] = '';
                    $order_data['shipping_postcode'] = '';
                    $order_data['shipping_zone'] = '';
                    $order_data['shipping_zone_id'] = '';
                    $order_data['shipping_country'] = '';
                    $order_data['shipping_country_id'] = '';
                    $order_data['shipping_address_format'] = '';
                    $order_data['shipping_custom_field'] = array();
                    $order_data['shipping_method'] = '';
                    $order_data['shipping_code'] = '';
					
					$order_data['comment'] = $this->request->post['comment'];
                    $order_data['total'] = $product_info['price'] * $quantity;
                    $order_data['affiliate_id'] = 0;
                    $order_data['commission'] = 0;
                    $order_data['marketing_id'] = 0;
                    $order_data['tracking'] = '';
                    $order_data['language_id'] = $this->config->get('config_language_id');
                    
                    $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
					$order_data['currency_code'] = $this->session->data['currency'];
					$order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
                    
                    $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

					if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
					} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
					} else {
						$order_data['forwarded_ip'] = '';
					}

					if (isset($this->request->server['HTTP_USER_AGENT'])) {
						$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
					} else {
						$order_data['user_agent'] = '';
					}

					if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
						$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
					} else {
						$order_data['accept_language'] = '';
					}
					
					$this->load->model('checkout/order');

					$order_id = $this->model_checkout_order->addOrder($order_data);
					
					if (empty($order_id)) {
						$json['error']['order'] = $this->language->get('error_order');
						
					} else {
                        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
						
                        $json['success'] = $order_id;
					}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function sendForm(){
		
		$json = array();
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')){
			
			$names = array();
			$values = array();
			$data = $this->request->post['data'];
				
			foreach($data as $key => $result){
					
				$names[] = $result['name'];
				$values[] = $result['value'];
					
			}
			
			$html = '<div style="width: 800px; font-family: Tahoma, sans-serif;">';
			$html .= '	<div style="padding: 10px; background-color: #f5b553; color: #fff; margin-bottom: 25px;">';
			$html .= '		Сообщение с сайта <a href="'. HTTPS_SERVER .'" style="color: #fff;">' . HTTPS_SERVER .'</a>';
			$html .= '	</div>';
			
			foreach($names as $key => $name){
				$html .= '	<div style="padding: 10px; margin-bottom: 10px;">';
				$html .= '		'. $name .':<br>';
				$html .= '		<strong>'. html_entity_decode($values[$key], ENT_QUOTES, 'UTF-8') .'</strong><br>';
				$html .= '	<hr></div>';
			}
			
			$html .= '	<div style="padding: 10px; margin-bottom: 15px; background-color: #cccccc61;">';
			$html .= '		Форма вызвана на странице:<br>';
			$html .= '		<a href="'. $this->request->post['url'] .'" style="color: #000;">'. $this->request->post['page'] .'</a>';
			$html .= '	</div>';
			
			$html .= '</div>';
			
		
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender('robot');
			$mail->setSubject('Сообщение с сайта: '.$this->config->get('config_name'));			
			$mail->setHtml($html);
			$mail->send();
			
			$json['success'] = 1;

		}else{
			$json['error'] = 1;
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));	
		
	}
}